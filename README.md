# Ningilin Types

Types associated with the [Ningilin](https://gitlab.com/srcrr/ningilin2) project.

To install using NPM:

    > npm i --save @types/ningilin

Note that of the writing of this README the types have not been incorporated into
[DefinitelyTyped](https://github.com/DefinitelyTyped/DefinitelyTyped).

If that's the case you can install it manually:

    > cd ~
    > git clone https://gitlab.com/srcrr/ningilin_types.git
    > cd <working_project>/node_modules/@types
    > ln -s ~/ningilin_types/ ningilin