import * as ningilin from "types_ningilin";
import { Schema, Model } from "mongoose";
import { Router } from "express";

let nm = new ningilin.NingilinModel("", new Schema({}), {}, "", [], [], "", false);
nm.getIdField(); $ExpectType string
nm.getModel(); $ExpectType Model
nm.getPath(); $ExpectType string
nm.getPluralPath(); $ExpectType string
nm.getRouter(); $ExpectType Router