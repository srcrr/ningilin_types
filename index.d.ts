import {Request, Response, Router} from "express";
import {Schema} from "mongoose";

declare type Hook = (req : Request, res : Response) => Promise<void>;
declare type InfoHook = () => string;
declare type ErrorHandler = (err : Error, req : Request, res : Response) => Promise<void>;
declare type Callable = (Hook | InfoHook | ErrorHandler);

declare class HookMap {
    constructor(hooks: Array<Callable>);
}

declare class NingilinModel {
    public getPath : InfoHook;
    public getPluralPath : InfoHook;
    public getModel : InfoHook;
    public getIdField : InfoHook;
    public getRouter : InfoHook;
    constructor (name: string, schema: Schema,
        hooks?: {[index: string]: Callable},
        path? : string,
        preHooks? : Array<Hook>,
        postHooks ? : Array<Hook>,
        collection ? : string,
        skipInit ? : boolean);
}